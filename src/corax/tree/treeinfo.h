#ifndef CORAX_TREE_TREEINFO_H_
#define CORAX_TREE_TREEINFO_H_

#include "corax/corax_common.h"

#define PLLMOD_TREEINFO_PARTITION_ALL -1

typedef struct treeinfo_edge
{
  unsigned int left_index;
  unsigned int right_index;
  unsigned int pmatrix_index;
  double       brlen;
} pllmod_treeinfo_edge_t;

typedef struct treeinfo_topology
{
  unsigned int            edge_count;
  unsigned int            brlen_set_count;
  unsigned int            root_index;
  pllmod_treeinfo_edge_t *edges;
  double **               branch_lengths;
} pllmod_treeinfo_topology_t;

typedef struct treeinfo
{
  // dimensions
  unsigned int tip_count;
  unsigned int partition_count;

  /* 0 = linked/shared, 1 = linked with scaler, 2 = unlinked */
  int     brlen_linkage;
  double *linked_branch_lengths;

  pll_unode_t *root;
  pll_utree_t *tree;

  unsigned int  subnode_count;
  pll_unode_t **subnodes;

  // partitions & partition-specific stuff
  pll_partition_t **partitions;
  double *          alphas;
  int *gamma_mode; /* discrete GAMMA rates computation mode (mean, median) */
  unsigned int **param_indices;
  int **         subst_matrix_symmetries;
  double **      branch_lengths;
  double *       brlen_scalers;
  double *       partition_loglh;
  int *          params_to_optimize;

  // partition that have been initialized (useful for parallelization)
  unsigned int      init_partition_count;
  unsigned int *    init_partition_idx;
  pll_partition_t **init_partitions;

  /* tree topology constraint */
  unsigned int *constraint;

  /* precomputation buffers for derivatives (aka "sumtable") */
  double **deriv_precomp;

  // invalidation flags
  char **clv_valid;
  char **pmatrix_valid;

  // buffers
  pll_unode_t **   travbuffer;
  unsigned int *   matrix_indices;
  pll_operation_t *operations;

  // partition on which all operations should be performed
  int active_partition;

  // general-purpose counter
  unsigned int counter;

  // parallelization stuff
  void *parallel_context;
  void (*parallel_reduce_cb)(void *, double *, size_t, int);
} pllmod_treeinfo_t;

typedef struct
{
  unsigned int  node_count;
  pll_unode_t **nodes;

  unsigned int  partition_count;
  unsigned int *partition_indices;

  pll_utree_t *tree;
  double **    probs;
} pllmod_ancestral_t;

PLL_EXPORT pllmod_treeinfo_t *pllmod_treeinfo_create(pll_unode_t *root,
                                                     unsigned int tips,
                                                     unsigned int partitions,
                                                     int brlen_linkage);

PLL_EXPORT
int pllmod_treeinfo_set_parallel_context(
    pllmod_treeinfo_t *treeinfo,
    void *             parallel_context,
    void (*parallel_reduce_cb)(void *, double *, size_t, int op));

PLL_EXPORT int
pllmod_treeinfo_init_partition(pllmod_treeinfo_t * treeinfo,
                               unsigned int        partition_index,
                               pll_partition_t *   partition,
                               int                 params_to_optimize,
                               int                 gamma_mode,
                               double              alpha,
                               const unsigned int *param_indices,
                               const int *         subst_matrix_symmetries);

PLL_EXPORT int pllmod_treeinfo_set_active_partition(pllmod_treeinfo_t *treeinfo,
                                                    int partition_index);

PLL_EXPORT int pllmod_treeinfo_set_root(pllmod_treeinfo_t *treeinfo,
                                        pll_unode_t *      root);

PLL_EXPORT
int pllmod_treeinfo_get_branch_length_all(const pllmod_treeinfo_t *treeinfo,
                                          const pll_unode_t *      edge,
                                          double *                 lengths);

PLL_EXPORT int pllmod_treeinfo_set_branch_length(pllmod_treeinfo_t *treeinfo,
                                                 pll_unode_t *      edge,
                                                 double             length);

PLL_EXPORT
int pllmod_treeinfo_set_branch_length_all(pllmod_treeinfo_t *treeinfo,
                                          pll_unode_t *      edge,
                                          const double *     lengths);

PLL_EXPORT
int pllmod_treeinfo_set_branch_length_partition(pllmod_treeinfo_t *treeinfo,
                                                pll_unode_t *      edge,
                                                int    partition_index,
                                                double length);

PLL_EXPORT
pll_utree_t *
pllmod_treeinfo_get_partition_tree(const pllmod_treeinfo_t *treeinfo,
                                   int                      partition_index);

PLL_EXPORT
pllmod_treeinfo_topology_t *
pllmod_treeinfo_get_topology(const pllmod_treeinfo_t *   treeinfo,
                             pllmod_treeinfo_topology_t *topol);

PLL_EXPORT
int pllmod_treeinfo_set_topology(pllmod_treeinfo_t *               treeinfo,
                                 const pllmod_treeinfo_topology_t *topol);

PLL_EXPORT
int pllmod_treeinfo_destroy_topology(pllmod_treeinfo_topology_t *topol);

PLL_EXPORT int pllmod_treeinfo_destroy_partition(pllmod_treeinfo_t *treeinfo,
                                                 unsigned int partition_index);

PLL_EXPORT void pllmod_treeinfo_destroy(pllmod_treeinfo_t *treeinfo);

PLL_EXPORT int pllmod_treeinfo_update_prob_matrices(pllmod_treeinfo_t *treeinfo,
                                                    int update_all);

PLL_EXPORT void pllmod_treeinfo_invalidate_all(pllmod_treeinfo_t *treeinfo);

PLL_EXPORT int pllmod_treeinfo_validate_clvs(pllmod_treeinfo_t *treeinfo,
                                             pll_unode_t **     travbuffer,
                                             unsigned int travbuffer_size);

PLL_EXPORT void pllmod_treeinfo_invalidate_pmatrix(pllmod_treeinfo_t *treeinfo,
                                                   const pll_unode_t *edge);

PLL_EXPORT void pllmod_treeinfo_invalidate_clv(pllmod_treeinfo_t *treeinfo,
                                               const pll_unode_t *edge);

PLL_EXPORT double pllmod_treeinfo_compute_loglh(pllmod_treeinfo_t *treeinfo,
                                                int                incremental);

PLL_EXPORT double pllmod_treeinfo_compute_loglh_flex(
    pllmod_treeinfo_t *treeinfo, int incremental, int update_pmatrices);

PLL_EXPORT double pllmod_treeinfo_compute_loglh_persite(
    pllmod_treeinfo_t *treeinfo, int incremental, double **persite_lnl);

PLL_EXPORT
int pllmod_treeinfo_scale_branches_all(pllmod_treeinfo_t *treeinfo,
                                       double             scaler);

PLL_EXPORT
int pllmod_treeinfo_scale_branches_partition(pllmod_treeinfo_t *treeinfo,
                                             unsigned int       partition_idx,
                                             double             scaler);

PLL_EXPORT
int pllmod_treeinfo_normalize_brlen_scalers(pllmod_treeinfo_t *treeinfo);

PLL_EXPORT int pllmod_treeinfo_set_tree(pllmod_treeinfo_t *treeinfo,
                                        pll_utree_t *      tree);

PLL_EXPORT int
pllmod_treeinfo_set_constraint_clvmap(pllmod_treeinfo_t *treeinfo,
                                      const int *        clv_index_map);

PLL_EXPORT int
pllmod_treeinfo_set_constraint_tree(pllmod_treeinfo_t *treeinfo,
                                    const pll_utree_t *cons_tree);

PLL_EXPORT int pllmod_treeinfo_check_constraint(pllmod_treeinfo_t *treeinfo,
                                                pll_unode_t *      subtree,
                                                pll_unode_t *regraft_edge);

PLL_EXPORT pllmod_ancestral_t *
           pllmod_treeinfo_compute_ancestral(pllmod_treeinfo_t *treeinfo);

PLL_EXPORT void
pllmod_treeinfo_destroy_ancestral(pllmod_ancestral_t *ancestral);

#endif /* CORAX_TREE_TREEINFO_H_ */
